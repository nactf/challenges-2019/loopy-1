CC=gcc
CFLAGS=-std=c90 -m32 -fstack-protector-all -no-pie -fno-pic -Wl,-rpath,.

.PHONY: default
default: loopy-1

%: %.c
	$(CC) -o $@ $< $(CFLAGS)
